@extends('layouts.app')
@section('content')

@include('partials.banners', ['class' => 'service-page-wrapper aditional-services-background'])
@include('partials/services', ['type' => 'additional'])

<div itemprop="mainContentOfPage">
    <div class="aditional-services-wrapper">
        <div class="container">
            @foreach($additional as $k => $v)
            <div class="row aditional-services-row">
                <div class="col">
                    <h2 class="h2-aditional-services">{{ $v->title }}</h2>
                    <p class="p-aditional-services">{{ $v->content }}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@include('partials.request')

@endsection