@extends('layouts.app')
@section('content')

  <div itemprop="mainContentOfPage">
  @foreach($services as $k => $v)
        <div class="services-page-wrapper"
             style="background: url({{Voyager::image($v->image)}}) no-repeat;background-size: cover;background-position: center center;">
            <div class="container">
                <div class="row defending-work-wrapper-row">
                    <div class="col-xl">
                        <h2>{{$v->name}}</h2>
                    </div>
                    <div class="col-sm">
                        <p>{{$v->short_description}}</p>
                        <a href="{{ route('inner_service', $v->url) }}"><button type="button">Подробнее об услуге</button></a>
                        <a href="{{ route('inner_service', $v->url) }}#programForm"><button type="button" class="services-second-button">Оставить заявку</button></a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
  </div>

@endsection
