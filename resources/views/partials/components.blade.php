<template id="firstProgramsComponent">
    <div class="cart-section">
        <div class="cart-section-number">
            <div class="cart-section-number-main-box first-cart-border">
                <div class="cart-section-number-child-box">
                    <div class="cart-section-number-first-value" >
                        100
                    </div>
                </div>
            </div>
        </div>
        <div class="cart-section-title">Двухчасовой инструктаж по охране труда</div>
        <div class="cart-section-description">Длительность курса: 2 часа</div>
    </div>
    <div class="cart-section-hover">
        <a href="/services/occupational-safety-and-health#1"> Подробнее об услуге </a>
        <img src="{{ asset("img/cart-section-hover-icon.png") }}" alt="">
    </div>
    <div class="cart-section-substrate"></div>
</template>


<template id="secondProgramsComponent">
    <div class="col">
        <div class="cart-section">
            <div class="cart-section-number">
                <div class="cart-section-number-main-box second-cart-border">
                    <div class="cart-section-number-child-box">
                        <div class="cart-section-number-second-value">
                            2
                        </div>
                    </div>
                </div>
            </div>
            <div class="cart-section-title">Двухчасовой инструктаж по охране труда</div>
            <div class="cart-section-description">Длительность курса: 2 часа</div>
        </div>
        <div class="cart-section-hover">
            <a href="/services/occupational-safety-and-health#1"> Подробнее об услуге </a>
            <img src="{{ asset("img/cart-section-hover-icon.png") }}" alt="">
        </div>
        <div class="cart-section-substrate"></div>
    </div>
</template>



<template id="thirdProgramsComponent">
    <div class="col">
        <div class="cart-section">
            <div class="cart-section-number">
                <div class="cart-section-number-main-box third-cart-border">
                    <div class="cart-section-number-child-box">
                        <div class="cart-section-number-third-value">
                            2
                        </div>
                    </div>
                </div>
            </div>
            <div class="cart-section-title">Двухчасовой инструктаж по охране труда</div>
            <div class="cart-section-description">Длительность курса: 2 часа</div>
        </div>
        <div class="cart-section-hover">
            <a href="/services/occupational-safety-and-health#1"> Подробнее об услуге </a>
            <img src="{{ asset("img/cart-section-hover-icon.png") }}" alt="">
        </div>
        <div class="cart-section-substrate"></div>
    </div>
</template>


<template id="containerComponent">
    <div class="row our-service-aditional-info">
        <div class="col">
            <div class="content">
                <h2>Что нужно знать о Охрана труда?</h2>
                <p>Промышленная безопасность – комплекс мероприятий, входящий в систему управления рисками на предприятии. Она подразумевает организацию условий труда на предприятии с минимальным риском возникновения аварий, с одной стороны, и разработку оптимального плана действий на случай аварии на производстве с целью минимизации негативных последствий для сотрудников и населения – с другой.</p>
            </div>
            <div class="url">
                <a href="/services/occupational-safety-and-health">Читать дальше <img src="{{ asset("img/our-service-aditional-info-link-icon.png") }}" alt=""></a>
            </div>
        </div>
    </div>
</template>
