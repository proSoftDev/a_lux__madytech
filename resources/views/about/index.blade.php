@extends('layouts.app')
@section('content')


    @include('partials.banners',['class' => "about-company-wrapper"])
    <div itemprop="mainContentOfPage">
    <div class="about-modal">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="about-company-name">
                        <h2>{{$about->title_top}}</h2>
                        <p>{{$about->content_top}}</p>
                    </div>
                    <div class="about-company-name-substrate"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="more-about-company">
                    <img src="{{Voyager::image($about->image)}}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col more-about-company-second-section">
                <h2>{{$about->title_mid}}</h2>
                {!!$about->content_mid!!}
            </div>
        </div>
    </div>
    <div class="strategy-row-wrapper">
        <div class="container">
            <div class="row our-strategy">
                @php $m=0 @endphp
                @foreach($types as $k => $v)
                @php $m++ @endphp
                <div class="col-sm">
                    <div class="strategy-wrapper">
                        <div class="strategy-number">0{{$m}}</div>
                        <p>{{$v->content}}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="gallery-wrapper">
        <div class="container">
            <div class="row about-company-gallery certificates-slider">
                <div class="col">
                    <h2>{!! $title[4]->getText() !!}</h2>
                    <div class="gallery">
                        @foreach($certificates as $k => $v)
                        <a class="magific-pop" href="{{Voyager::image($v->image)}}">
                            <div class="gallery-slider">
                                <img class="gallery-item" src="{{Voyager::image($v->image)}}">
                            </div>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- лого клиентов -->
    <div class="container">
        <div class="row happy-clients-row">
            <div class="col-8 happy-clients">
                <h2>{!! $title[3]->getText() !!}</h2>
            </div>
        </div>

        @include('partials.clients', compact('clients'))

    </div>
</div>
@endsection

