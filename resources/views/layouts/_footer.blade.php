
<footer>
    <div class="container">
        <div class="row footer-row">
            <div class="col-sm-3 col-md-3">
                <a href=""><img src="{{ asset("img/logo-footer.png") }}" class="img-fluid"></a>
                <nav class="flex-column footer-row-links">
                    {{--<ul class="p-0 ul_non">--}}
                        {{--<li><a class="nav-link" href="#">Наши реквизиты</a></li>--}}
                        {{--<li><a class="nav-link" href="#">Карта сайта</a></li>--}}
                    {{--</ul>--}}
                    
                </nav>
            </div>
            <div class="col-sm-3 col-md-3">
                <h3>Наши услуги</h3>
                <nav class="footer-row-links">
                <ul class="p-0 ul_non">
                    @foreach($services as $k => $v)
                        
                            <li><a class="nav-link" rel="nofollow" href="{{ route('inner_service', $v->url) }}">{{$v->name}}</a></li>
                         
                    @endforeach
                </ul> 
                </nav>

            </div>
            <div class="col-sm-3 col-md-3">
                <h3>Разделы</h3>
                <nav class="footer-row-links">
                <ul class="p-0 ul_non">
                    @foreach (menu('footer', '_json') as $menuItem)
                        <li><a class="nav-link" rel="nofollow" href="{{ $menuItem->url }}">{{ $menuItem->title}}</a></li>
                    @endforeach
                </ul>
                </nav>
            </div>
            <div class="col-sm-3 col-md-3">
                <h3>Контакты</h3>
                <div class="footer-row-contacts">
                    <div class="footer-row-contacts-email"><a
                                href="mailto: maditechnoexpert@gmail.com">{{setting('site.email')}}</a></div>
                    <div class="ooter-row-contacts-phone">
                        <a href="tel:{{setting('site.telephone')}}">{{setting('site.telephone')}}</a></div>
                    <div class="ooter-row-contacts-address">{{setting('site.address')}}</div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copyright-wrapper">
    <div class="container">
        <div class="row copyright-row">
            <div class="col-xl-10">
                <div class="copyright">
                    <p>{{setting('site.copyright')}}</p>
                </div>
            </div>
            <div class="col-xl-2">
                <div class="copyright">
                    <p>Разработано в <a href="https://www.a-lux.kz/" target="_blank" rel="nofollow"> A-LUX</a> </p>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{ asset('fonts/fonts.css') }}">
<link rel="stylesheet" href="{{ asset('css/slick.css') }}">
<link rel="stylesheet" href="{{ asset('css/carousel.css') }}">
<link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css')}}">
<script src="{{ asset('js/jquery-3.4.1.min.js')}}"></script>
<script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{ asset('js/sweetalert2.min.js')}}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('js/slick.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js')}}"></script>
<script src="{{ asset('js/carousel.js')}}"></script>
<script src="{{ asset('js/ajax.js') }}"></script>

@stack('scripts')
</body>

</html>
