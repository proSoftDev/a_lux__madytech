<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" itemscope itemtype="https://schema.org/WebPage">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="{{ $page->getMeta()->description}}">
    <meta name="keyword" content="{{ $page->getMeta()->keyword }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $page->getMeta()->title }}</title>
    <meta name="base" content="{{ url('/') }}">
    <meta name="google-site-verification" content="fRq5kTbjHue4zYI_jVOTBbMsCP6McLCmNiQMCN_ts7k" />
    @if($_SERVER["REQUEST_URI"] == '/produktsiya' || $_SERVER["REQUEST_URI"] == '/blog')
    <meta name="robots" content="noindex, follow">
    @elseif(Illuminate\Support\Facades\Request::segment(1) == 'produktsiya' && Illuminate\Support\Facades\Request::segment(2) != null)
    <meta name="robots" content="noindex">
    @endif
    <link rel="shortcut icon" href="{{ asset("/img/favicon.ico") }}" type="image/x-icon">
    <link rel="icon" href="{{ asset("/img/favicon.ico") }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">


    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function(m, e, t, r, i, k, a) {
            m[i] = m[i] || function() {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(47763031, "init", {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true
        });
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/47763031" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114450318-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-114450318-1');
    </script>
</head>

<body>
    <div class="header-wrapper">
        <header class='head-desctop'>
            <div class="container-fluid">
                <div class="row head-row">
                    <div class="col-xl-2 logo">
                        <? if ($_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/site/index") : ?>
                            <img src="{{Voyager::image(setting('site.logo'))}}" class="img-logo" alt="logo">
                        <? else : ?>
                            <a href="{{ route('home') }}">
                                <img src="{{Voyager::image(setting('site.logo'))}}" class="img-logo" alt="logo">
                            </a>
                        <? endif; ?>
                    </div>
                    <div class="col-xl-1 menu-button-column">

                        <div class="dropdown menu-button">
                            <button class="btn btn-secondary menu-button text-sm-right pr-4" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{ asset("img/menu-list.png") }}" class="menu-list" alt="">
                                Меню
                            </button>
                            <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                                @foreach (menu('header', '_json') as $menuItem)
                                <a class="dropdown-item" href="{{ $menuItem->url }}">{{ $menuItem->title}}</a>
                                @endforeach
                            </div>
                        </div>

                    </div>
                    <div class="col-xl-4 address">
                        <p>{{ setting('site.address') }}</p>
                        <div class="address-map-link"><img src="{{ asset("img/map.svg") }}" alt="">
                            <a href="{{ route('contact') }}">Смотреть на карте</a>
                        </div>
                        <!-- <div class="schedule-info"><img src="{{ asset("img/clock.svg") }}" alt=""><a href="#">График работы</a></div>
                    </div> -->
                        <a class="btn" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                            График работы
                        </a>
                        <div class="collapse schedule-collapse" id="collapseExample">
                            <div class="card card-body schedule-card">
                                {!! setting('site.schedule') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 phone">
                        <a href="mailto: maditechnoexpert@gmail.com">maditechnoexpert@gmail.com</a>
                        <a href="tel:{{ setting('site.telephone') }}">{{ setting('site.telephone') }}</a>
                        <div class="phone-icon" data-toggle="modal" data-target="#feedbackModal">
                            <img src="{{ asset("img/smartphone.svg") }}" alt="">Заказать звонок</div>
                    </div>
                    <div class="mobile-menu">

                        <!--Navbar-->
                        <nav class="navbar navbar-dark  indigo darken-2">



                            <!-- Collapse button -->
                            <button class="navbar-toggler third-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent22" aria-controls="navbarSupportedContent22" aria-expanded="false" aria-label="Toggle navigation">
                                <div class="animated-icon3">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </button>

                            <!-- Collapsible content -->
                            <div class="collapse navbar-collapse" id="navbarSupportedContent22">

                                <!-- Links -->
                                <ul class="navbar-nav mr-auto">

                                    @foreach (menu('header', '_json') as $menuItem)
                                    <li class="nav-item {{$_SERVER["REQUEST_URI"] == $menuItem->url ? "active":""}}">
                                        <a class="nav-link" href="{{ $menuItem->url }}">
                                            {{ $menuItem->title}}
                                            {{$_SERVER["REQUEST_URI"] == $menuItem->url ? '<span class="sr-only">(current)</span>':''}}
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                                <!-- Links -->

                            </div>
                            <!-- Collapsible content -->

                        </nav>
                        <!--/.Navbar-->

                    </div>
                    <div class="col-xl-2 call-us">
                        <a href="tel:{{ setting('site.telephone') }}">
                            <button type="button" class="">
                                Позвонить нам
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </header>
        <div class='mobile-logo'>
            <? if ($_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/site/index") : ?>
                <img src="{{Voyager::image(setting('site.logo'))}}" class="img-mob" alt="logo">
            <? else : ?>
                <a href="{{ route('home') }}">
                    <img src="{{Voyager::image(setting('site.logo'))}}" class="img-mob" alt="logo">
                </a>
            <? endif; ?>
        </div>
        <div class="mobile-contacts">
            <a href="tel:{{ setting('site.telephone') }}">{{ setting('site.telephone') }}</a>
            <a href="mailto: maditechnoexpert@gmail.com">{{setting('site.email')}}</a>
        </div>
        <div class="burger-menu">
            <a href="#" class="burger-menu-btn">
                <span class="burger-menu-lines"></span>
            </a>
        </div>

        <div class="nav-panel-mobil">
            <div class="container">
                <nav class="navbar-expand-lg navbar-light">
                    <ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
                        @foreach (menu('header', '_json') as $menuItem)
                        <li class="nav-item">
                            <a class="nav-link item icons" href="{{ $menuItem->url }}">
                                {{ $menuItem->title}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
        </div>
    </div>