@extends('layouts.app')
@section('content')

@include('partials.banners', ['class' => 'service-page-wrapper laws-wrapper'])

<div itemprop="mainContentOfPage">
    <div class="container">
        @php $m = 0 @endphp
        @foreach($model as $k => $v)
        @php $m++; @endphp
        @php $types = $v->laws; @endphp
        <div class="row {{count($model) == $m ? "laws-margin" : ""}}">
            <div class="col laws-list">
                <h2 class="h2-normal-style">{{$v->name}}</h2>
                <ul>
                    @foreach($types as $type)
                    <li>
                        <a href="{{$type->url}}" target="_blank" rel="nofollow">{{$type->name}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection