@extends('layouts.app')
@section('content')

@include('partials.banners',['class' => "service-page-wrapper blog-wrapper"])
<div itemprop="mainContentOfPage">

    <div class="container news-contariner">



        <div class="owl-carousel">
            @foreach($model as $k => $v)
            <div class="card">
                <img class="card-img-top" src="{{ Voyager::image($v->image) }}" alt="" >
                <div class="card-body" style="height: 380px;">
                    <h5 class="card-title">{{ $v->name }}</h5>
                    <p class="card-text">{!! $v->short_description !!}</p>
                    @if($v->source)
                    <p class="card-source">
                        <a href="{{ $v->source }}" target="_blank"> Источник: {{ $v->source }}</a>
                    </p>
                    @endif
                </div>
                <div class="card-footer">
                    <a href="{{ route('inner_blog', $v->url) }}"><button type="button">Читать статью <img src="{{ asset("img/blog-right-arrow.png") }}" alt=""></button></a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>

@include('partials.request')
@endsection
