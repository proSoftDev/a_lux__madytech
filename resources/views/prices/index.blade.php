@extends('layouts.app')
@section('content')

@include('partials.banners', ['class' => 'service-page-wrapper price-background'])
@include('partials/services', ['type' => "prices"])

<div itemprop="mainContentOfPage">
    <div class="container">
        @foreach($prices as $k => $v)
        <div class="row price-row">
            <div class="col">
                <h2 class="h2-normal-style price-h2">{{$v->title}}</h2>
                @if($v->subtitle)
                <p style="text-align: center;" class="p-normal-style">{{$v->subtitle}}</p>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="divTable blueTable">
                <div class="divTableHeading">
                    <div class="divTableRow head-row">
                        <div class="divTableHead table-first-row">Наименование</div>
                        <div class="divTableHead" style="text-align: center;">еД. ИЗМЕРЕНИЯ</div>
                        <div class="divTableHead" style="text-align: center;">цена, без учёта ндс</div>
                    </div>
                </div>
                <div class="divTableBody">
                    @php $names = $v->names; @endphp
                    @foreach($names as $k1 => $v1)
                    @php $prices = $v1->prices; @endphp
                    @php $units = $v1->units; @endphp
                    <div class="divTableRow">
                        <div class="divTableCell table-first-row">{{$v1->name}}</div>
                        @if(count($units) > 1)
                        <div class="divTableCell table-cell-center">
                            @foreach($units as $k2 => $v2)
                            <div class="extra-cell">{!! $v2->unit !!}</div>
                            @endforeach
                        </div>
                        @elseif(count($units) == 1)
                        <div class="divTableCell table-cell-center">{!! $units[0]->unit !!}</div>
                        @endif

                        @if(count($prices) > 1)
                        <div class="divTableCell table-cell-center">
                            @foreach($prices as $k2 => $v2)
                            <div class="extra-cell">{{$v2->price}}</div>
                            @endforeach
                        </div>
                        @elseif(count($prices) == 1)
                        <div class="divTableCell table-cell-center">{{$prices[0]->price}}</div>
                        @endif

                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach

        @include('partials.request')
    </div>

</div>
@endsection