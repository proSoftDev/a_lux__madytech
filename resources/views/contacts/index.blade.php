@extends('layouts.app')
@section('content')

@include('partials.banners',['class' => "service-page-wrapper contacts-background"])
<div itemprop="mainContentOfPage">
    <div class="contacts-map-wrapper">

        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A54dec1aaa00f6942a0c8ab7743cf6197029c5ce17565c318481d9138a94bc179&amp;source=constructor" width="100%" height="500" frameborder="0"></iframe>
        <div class="contacts-section-title">Наш адрес</div>
    </div>
    <div class="contacts-information-substrate">
        <div class="col-xs">
            <div class="contacts-section">
                <img src="{{ asset("img/contacts-email.jpg") }}" alt="">
                <div class="contacts-section-title">Наш адрес</div>
                <p class="p-contacts-style">
                    {{setting('site.address')}}
                </p>
            </div>
        </div>
        <div class="col-xs">
            <div class="contacts-section">
                <img src="{{ asset("img/contacts-email.jpg") }}" alt="">
                <div class="contacts-section-title">Электронная почта</div>
                <p class="p-contacts-style">
                    <a href="mailto:{{setting('site.email')}}">{{setting('site.email')}}</a>
                </p>
            </div>
        </div>
        <div class="col-xs">
            <div class="contacts-section">
                <img src="{{ asset("img/contacts-phone.jpg") }}" alt="">
                <div class="contacts-section-title">тЕЛЕФОН:</div>
                <p class="p-contacts-style">
                    <a href="tel:{{setting('site.telephone')}}">{{setting('site.telephone')}}</a>
                </p>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@include('partials.request')

@endsection
