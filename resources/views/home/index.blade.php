@extends('layouts.app')
@section('content')

<div itemprop="mainContentOfPage" >
    <div class="first-screen-wrapper" style="background: url({{Voyager::image($banner->banner_image)}}) no-repeat;">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="first-screen-description">
                        <div class="first-screen-label"><img src="{{Voyager::image($banner->price_image)}}" alt=""></div>
                        <div class="first-screen-text">
                            {!! $banner->title !!}
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col">
                    <div class="first-screen-second-description">
                        {!! $banner->subtitle!!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col first-screen-cost">
                    <a href="{{$banner->url}}">
                        <button type="button" class="first-screen-button">
                            Расчитать стоимость
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container benefits">
        <div class="row benefits-first-section">
            <!-- непосредственно блоки приемущств -->
            @foreach($company_numbers as $k => $v)
            <div class="col benefit-parent">
                <div class="benefit-image"><img src="{{ asset("img/benefit-defend-objects.png") }}" alt=""></div>
                <div class="benefit-number">{{$v->quantity}}</div>
                <div class="benefit-name">{{$v->text}}</div>
            </div>
            @endforeach
        </div>
        <!-- подложки/substrates -->
        <div class="row benefits-second-substrate"></div>
        <div class="row benefits-third-substrate"></div>
    </div>

    <div class="container certificates">
        <div class="row our-certificates">
            <div class="col our-certificates-button">
                <button type="button" class="">Наши сертификаты</button>
            </div>
        </div>
        <div class="row high-certificates">
            <div class="col-xl-3 certificates-extinguisher">
                <img src="{{ asset("img/image17.png") }}" alt="">
            </div>
            <div class=" col-xl-6 col-md-12">
                <h2>{!! $title[0]->getText() !!}</h2>
            </div>
            <div class="col-xl-3 certificates-helmet">
                <img src="{{ asset("img/certificates-helmet.png") }}" alt="">
            </div>
        </div>
        <!-- наши сертификаты -->
        <div class="row about-company-gallery certificates-slider">
            <div class="col">
                <h2>{!! $title[4]->getText() !!}</h2>
            </div>
        </div>
    </div>
    <div class="gallery">
        @foreach($certificates as $k => $v)
        <a class="magific-pop" href="{{Voyager::image($v->image)}}">
            <div class="gallery-slider">
                <img class="gallery-item" src="{{Voyager::image($v->image)}}">
            </div>
        </a>
        @endforeach
    </div>
</div>
</div>
</div>


<!-- приемущества наличия сертификатов -->
<div class="other-benefits-first-wrapper">
    <div class="other-benefits-second-wrapper" style="background: url({{Voyager::image($advantage_con->background)}})">
        <div class="container other-benefits">
            <div class="row">
                <div class="col benefits-of-having">
                    <h2>{!! $title[1]->getText() !!}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-md-6 other-benefits-first">
                    @php $m = 0; @endphp
                    @foreach($advantages as $k => $v)
                    @if($m < 2) <div class="other-benefits-sections">
                        <div class="other-benefits-sections-image">
                            <img src="{{Voyager::image($v->icon)}}" alt="">
                        </div>
                        <div class="other-benefits-sections-description">
                            {{$v->content}}
                        </div>
                </div>
                @endif
                @php $m++; @endphp
                @endforeach
            </div>
            <div class="col-xl-3 col-md-6 other-benefits-second-sections">
                @php $m = 0; @endphp
                @foreach($advantages as $k => $v)
                @php $m++; @endphp
                @if($m > 2)
                <div class="other-benefits-sections">
                    <div class="other-benefits-sections-image">
                        <img src="{{Voyager::image($v->icon)}}" alt="">
                    </div>
                    <div class="other-benefits-sections-description">
                        {{$v->content}}
                    </div>
                </div>
                @endif
                @endforeach
            </div>
            <div class="col-xl-6 col-md-12 other-benefits-sertificates">
                <img src="{{Voyager::image($advantage_con->certificate)}}" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</div>
</div>
<div class="container our-service">
    <div class="row our-service-row">
        <div class="col-9 our-service-row-first-section">
            <h2>{!! $title[2]->getText() !!}</h2>
        </div>
        <div class="col-3 our-service-row-second-section">
            <div class="our-service-row-icon">
                <img src="{{ asset("img/our-service-row-icon.png") }}" alt="">
            </div>
            Кликайте на нужную услугу
        </div>
    </div>

    <!-- спектр услуг -->
    <div class="row choose-service-section">
        @php $class = "active"; @endphp
        @foreach($services as $k => $v)
        <div class="col">
            <div class="service">
                <button class="{{ $class }}" onclick="showStudyingProgram({{$v->id}})" id="service_{{ $v->id }}">
                    <div class="service-description">{{ $v->name }}</div>
                    <div class="service-image service-image-addition-margin"><img src="{{ Voyager::image($v->icon) }}" alt=""></div>
                </button>
            </div>
        </div>
        @if($class == "active") @php $class = ""; $studying_programs = $v->studying_programs @endphp @endif
        @endforeach
    </div>
    <!-- курсы, программы обучения -->
    <div class="row carts-wrapper">
        <div class="studying-programs">программы обучения</div>
        @php $m = 0; @endphp
        @foreach($studying_programs as $k => $v)
        @php $m++; @endphp
        @if($m > 3) @break @endif
        <div class="col" id="studying_program_{{ $m }}">
            <div class="cart-section">
                <div class="cart-section-number">
                    <div class="cart-section-number-main-box {{ numberToWords($m) }}-cart-border">
                        <div class="cart-section-number-child-box">
                            <div class="cart-section-number-{{ numberToWords($m) }}-value">
                                {{ $v->duration }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cart-section-title">{{ $v->name }}</div>
                <div class="cart-section-description">Длительность курса: {{ $v->duration }} часа</div>
            </div>
            <div class="cart-section-hover">
                <a href="{{ route('inner_service',  $services[0]->url.'#'.$v->id) }}"> Подробнее об услуге </a>
                <img src="{{ asset("img/cart-section-hover-icon.png") }}" alt="">
            </div>
            <div class="cart-section-substrate"></div>
        </div>
        @endforeach
    </div>
    <div class="container" id="serviceContent">
        <div class="row our-service-aditional-info">
            <div class="col">
                <div class="content">
                    {!! $services[0]->main_short_description !!}
                </div>
                <div class="url">
                    <a href="{{ route('inner_service', $services[0]->url) }}">Читать дальше <img src="{{ asset("img/our-service-aditional-info-link-icon.png") }}" alt=""></a>
                </div>
            </div>
        </div>
    </div>


</div>


<!-- лого клиентов -->
<div class="container">
    <div class="row happy-clients-row">
        <div class="col-8 happy-clients">
            <h2>{!! $title[3]->getText() !!}</h2>
        </div>
    </div>
    @include('partials.clients', compact('clients'))
</div>
</div>

@include('partials.components')
@endsection