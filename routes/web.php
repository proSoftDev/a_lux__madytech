<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/prices', 'PriceController@index')->name('price');
Route::get('/zakony', 'LawController@index')->name('law');
Route::get('/contacts', 'ContactController@index')->name('contact');
Route::get('/produktsiya', 'ServiceController@all')->name('all_service');
Route::get('/produktsiya/{url}', 'ServiceController@index')->name('inner_service');
Route::get('/blog', 'BlogController@all')->name('all_blog');
Route::get('/{url}', 'BlogController@index')->name('inner_blog');

// Request
Route::post('/service/show-studying-program', 'ServiceController@showStudyingProgram');
Route::post('/request/site', 'RequestController@site');
Route::post('/request/feedback', 'RequestController@feedback');
Route::post('/request/service', 'RequestController@service');


