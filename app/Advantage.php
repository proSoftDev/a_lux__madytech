<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Advantage extends Model
{
    public static function getAll(){
        return Advantage::orderBy('sort', 'ASC')->get();
    }
}
