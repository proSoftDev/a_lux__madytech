<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ServiceAdvantage extends Model
{
    public static function getByService($service_id){
        return ServiceAdvantage::where('service_id', $service_id)->orderBy('sort', 'ASC')->get();
    }
}
