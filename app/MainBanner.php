<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MainBanner extends Model
{
    public static function getContent(){
        return MainBanner::first();
    }
    
}
