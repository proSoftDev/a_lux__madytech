<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ServiceStage extends Model
{
    public static function getByService($service_id){
        return ServiceStage::where('service_id', $service_id)->orderBy('sort', 'ASC')->get();
    }
    
}
