<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.02.2020
 * Time: 14:25
 */

namespace App\Http\Controllers;


use App\AdditionalService;
use App\Service;
use App\ServiceAdvantage;
use App\ServiceBlock;
use App\ServiceStage;
use App\ServiceStudyingProgram;
use App\ServiceStudyingProgramContent;
use Illuminate\Http\Request;


class ServiceController extends Controller
{
    public function index($url){

        $service = Service::getByUrl($url);
        $additional = AdditionalService::getByUrl($url);
        if($service){
            $blocks = ServiceBlock::getByService($service->id);
            $studying_program_con = ServiceStudyingProgramContent::getContent($service->id);
            $studying_programs = ServiceStudyingProgram::getByService($service->id);
            $advantages = ServiceAdvantage::getByService($service->id);
            $stages = ServiceStage::getByService($service->id);
            return view('services.index', compact('service', 'blocks', 'studying_programs', 'advantages', 'stages', 'studying_program_con'));
        }else if($additional){
            return view('services.additional', compact('additional'));
        }else{
            abort(404);
        }
    }

    public function all(){

        $services = Service::getAll();
        return view('services.all', compact('services'));

    }

    public function showStudyingProgram(Request $request){
        $model = ServiceStudyingProgram::where("service_id", $request->id)->get();
        $service = Service::getByID($request->id);
        if($model && $service){
            $response = array();$studying_programs = array();$m = 0;
            foreach ($model as $k => $v){
                if($m < 3) $studying_programs[] = ['id' => $v->id,'name' => $v->name, 'quantity' => $v->duration];
                $m++;
            }

            $response["studying_programs"] = $studying_programs;
            $response["container"] = ["main_short_description" => $service->main_short_description, 'url' => $service->url];
            return response($response, 200);
        }
    }
}
