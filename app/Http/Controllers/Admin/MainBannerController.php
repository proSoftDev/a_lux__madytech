<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 17.01.2020
 * Time: 16:50
 */

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class MainBannerController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }

}
