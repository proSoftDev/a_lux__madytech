<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.02.2020
 * Time: 14:22
 */

namespace App\Http\Controllers;


use App\LawType;

class LawController extends Controller
{
    public function index(){

        $model = LawType::getAll();
        return view('laws.index', compact('model'));
    }

}
