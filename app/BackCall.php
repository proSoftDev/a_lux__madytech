<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BackCall extends Model
{
    protected $fillable = ['name', 'telephone'];
}
