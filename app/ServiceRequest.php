<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ServiceRequest extends Model
{
    protected $fillable = ['fio', 'email', 'training_purpose', 'service_studying_program_id'];

    public function serviceStudyingProgram(){
        return $this->belongsTo(ServiceStudyingProgram::class);
    }
}
