<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ServiceStudyingProgramContent extends Model
{

    public static function getContent($service_id){
        return ServiceStudyingProgramContent::where("service_id", $service_id)->first();
    }
}
