<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PriceType extends Model
{

    public static function getAll(){
        return PriceType::orderBy('sort', 'ASC')->with('names')->get();
    }

    public function names() {
        return $this->hasMany(Price::class)->with('prices', 'units')->orderBy("sort", 'ASC');
    }


}
