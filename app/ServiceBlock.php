<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ServiceBlock extends Model
{
    public static function getByService($service_id){
        return ServiceBlock::where('service_id', $service_id)->orderBy('sort', 'ASC')->get();
    }
    
}
