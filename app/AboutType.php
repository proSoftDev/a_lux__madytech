<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AboutType extends Model
{
    public static function getAll(){
        return AboutType::orderBy('sort', 'ASC')->get();
    }
    
}
