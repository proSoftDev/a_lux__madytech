<?php

namespace App;

use App\Page\IPage;
use Illuminate\Database\Eloquent\Model;


class Service extends Model implements IPage
{
    public static function getAll(){
        return Service::orderBy('sort', 'ASC')->get();
    }

    public static function getByUrl($url){
        return Service::where("url", $url)->first();
    }

    public static function getByID($url){
        return Service::where("id", $url)->first();
    }

    public static function getAllWithStudyingPrograms(){
        return Service::orderBy('sort', 'ASC')->with("studying_programs")->get();
    }

    public function studying_programs() {
        return $this->hasMany(ServiceStudyingProgram::class);
    }

    
}
