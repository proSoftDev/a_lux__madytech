<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class About extends Model
{
    public static function getContent(){
        return About::first();
    }
}
