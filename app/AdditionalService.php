<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AdditionalService extends Model
{
    const url = "additional";
    public static function getByUrl($url){
        return $url == self::url ? AdditionalService::orderBy('sort', 'ASC')->get() : false;
    }
    
}
