$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});




function showStudyingProgram(id){

    $(".service .active").removeClass("active");
    $("#service_"+id).addClass("active");
    $.ajax({
        type: 'POST',
        url: '/service/show-studying-program',
        data: {id:id},
        dataType: "Json",
        success: function (data) {
            var studying_programs = data.studying_programs;
            var container = data.container;
            var programComponent, containerComponent, className = 'first';

            for(var i = 0;i < studying_programs.length;i++){
                if(i == 0) className = 'first';
                if(i == 1) className = 'second';
                if(i == 2) className = 'third';
                document.getElementById('studying_program_'+(i+1)).innerHTML ="";

                programComponent = document.querySelector('#'+className+'ProgramsComponent');
                programComponent.content.querySelector('.cart-section-number-'+className+'-value').innerHTML = studying_programs[i].quantity;
                programComponent.content.querySelector('.cart-section-title').innerHTML = studying_programs[i].name;
                programComponent.content.querySelector('a').href = '/services/'+container.url+'#'+studying_programs[i].id;
                programComponent = document.importNode(programComponent.content, true);

                document.getElementById('studying_program_'+(i+1)).appendChild(programComponent);
            }


            document.getElementById('serviceContent').innerHTML ="";
            containerComponent = document.querySelector('#containerComponent');
            containerComponent.content.querySelector('.content').innerHTML = container.main_short_description;
            containerComponent.content.querySelector('a').href = '/services/'+container.url;
            containerComponent = document.importNode(containerComponent.content, true);
            document.getElementById('serviceContent').appendChild(containerComponent);
        },
        error: function () {

        }
    });
}


function showProgramForm(id){
    document.getElementById("programForm").scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'start' });
    document.getElementById("inputPersonalEducation").value = id;
}



$('body').on('click','#feedbackBtn', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/feedback',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                $("#feedbackModal form")[0].reset();
                $("#feedbackModal").modal("hide");
                Swal.fire('Заявка ушпешно отправлен!', 'В ближайщее время мы свяжемся с вами.', 'success');
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});



$('body').on('click','#requestBtn', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/site',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                $(".form-wrapper form")[0].reset();
                Swal.fire('Заявка ушпешно отправлен!', 'В ближайщее время мы свяжемся с вами.', 'success');
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});



$('body').on('click','#serviceRequestBtn', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/service',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                Swal.fire('Заявка ушпешно отправлен!', 'В ближайщее время мы свяжемся с вами.', 'success');
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});
