<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 16.02.2020
 * Time: 14:37
 */

function numberToWords($num)
{
    $ones = array(
        1 => "first",
        2 => "second",
        3 => "third",
        4 => "fourth",
        5 => "fifth",
    );

    return $ones[$num];
}
